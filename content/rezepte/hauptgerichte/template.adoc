---
title: "Template Dish"
draft: true
kategorien: ["Hauptgericht"]
tags: ["tag1", "tagb", "tagc", "fnord", "bar"]
ernährung: ["vegetarisch"]
region: "Land/Region"
---

Kurzer Einleitungstext der ein bisschen über das Gericht erzählt.
Kann auch mal sein, dass ich eine persönliche Geschichte dazu schreibe.
Oder wieso ich das Gericht besonders mag.
Ich muss aber mehr Rezepte hier veröffentlichen.

<!--more-->

== Zutaten

- 2cl Zutat
- 1 EL Salz
- 1l Wasser

== Anleitung

. Wasser kochen
. Kochendes Wasser salzen
. Zutat dazu geben und 10 Minuten köcheln lassen.
