---
title: "Tofu Süss-Sauer"
draft: true
kategorien: ["Hauptgericht"]
tags: ["tofu", "suess-sauer", "asiatisch", "ananas"]
ernährung: ["vegetarisch"]
region: "Asien"
---

Das Rezept stammt von jessicainthekitchen.com und ich habe es nur an die 
in Deutschland in der SI-Welt üblichen Maße angepasst.

<!--more-->

== Zutaten

=== knusprig gebratener Tofu

- 450g fester (firm oder extra firm) Tofu
- 1 El Sojasoße
- 1/2 Tl Knoblauchpulver
- 1/4 Tl Salz
- 1/4 Tl schwarzer Pfeffer (gemahlen)
- 75g Stärke (z.B. Kartoffel- oder Maisstärke)
- 4 El Öl zum Braten

=== Süss-Saure Sauce

- 100g Zucker
- 70g Ketchup
- 60g weißer Essig oder Reisessig
- 60g Wasser
- 1 El Sojasoße
- 1 Tl Knoblauchpulver
- 125g frische Ananas, in ca. 1 x 2 cm große Würfel geschnitten
- 1 mittelgroße, rote Paprika, in mundgerechte Stücke geschnitten
- 1 mittelgroße Zwiebel, in ca 1 cm große Stücke geschnitten

=== Garnitur

- Sesam
- in feine Ringe geschnittene Frühlingszwiebel

== Zubereitung

=== knusprig gebratener Tofu
. Den Tofu in ca. 2 x 2 cm große Stücke reißen.
. In einer Schüssel den Tofu mit der Sojasoße vermischen, so dass der gesammte 
Tofu mit Sojasoße bedeckt ist.
. Knoblauchpulver, Salz und Pfeffer dazu geben und vorsichtig mischen.
. Die Stärke in eine weitere Schüssel geben und die gewürzten Tofuwürfel in
der Stärke wenden bis sie vollständig mit Stärke bedeckt sind. Aber Achtung,
die Stärke nimmt Feuchtigkeit vom Tofu auf wird dadurch transparent und 
schwierig zu sehen.
. Das Öl in einer Pfanne erhitzen und die Tofuwürfel darin braten bis sie eine
goldene Bräune haben.
. Tofu aus der Pfanne nehmen und zur Seite stellen.

=== Süss-Saure Sauce und finale Zubereitung
. In einer Schüssel Ketchup, Zucker, Essig, Wasser, Sojasoße und 
Knoblauchpulver vermischen.
. In der Pfanne in der der Tofu gebraten wurde, auf mittlerer bis hoher 
Temperatur die Zwiebel, Paprika und Ananas braten, bis die Zwiebeln glasig 
werden und die Ananasstücke anfangen zu Bräunen (ca. 5 Minuten).
. Die "Ketchupsauce" mit in die Pfanne geben und ca. 5 Minuten auf mittlerer
Hitze simmern lassen, bis die Sauce die gewünschte, leicht dicke und klebrige
Konsistenz erreicht hat.
. Den Tofu dazu geben, kurz umrühren und fertig. 
. Mit Sesam und in Ringe geschnittener Frühlingszwiebel garnieren. 
