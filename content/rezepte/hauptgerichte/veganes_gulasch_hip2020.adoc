---
title: "veganes Gulasch à la HIP2020"
draft: false
kategorien: ["Hauptgericht"]
tags: ["vegan", "tomate", "soja", "zwiebel", "paprika"]
ernährung: ["vegan"]
region: "Ungarn"
---

Dieses Gulasch ist wurde so auf der link:https://hackimpott.de[Hack im Pott] 2020 gekocht. 
Allerdings in einem deutlich größerem Maßstab.
Hier nun mein Versuch das Rezept auf 4 Portionen herunter zu skalieren.
Der Test des skalierten Rezepts steht allerdings noch aus.

<!--more-->

== Zutaten

=== Sojaschnetzel

- 150g Sojaschnetzel (roh, trocken)
- 250ml trockener Rotwein footnote:[Wein wird oft mit Hausenblase oder Gelatine geklärt. Falls relevant, unbedingt drauf achten einen veganen Wein zu bekommen.]
- 500ml Gemüsebrühe
- 2 EL Sojasauce
- 3-4 EL Sonnenblumenöl
- 1-2 Spritzer flüssiger Rauch

=== Gulasch
- die angebratenen Sojaschnetzel
- die augefangene Brühe vom Einweichen der Sojaschnetzel
- 1-2 EL Worcestersauce footnote:[Original Worcestersauce enthält Sardellen. Es gibt allerdings rein pflanzliche Versionen.]
- 250ml Gemüsebrühe
- 500ml passierte Tomaten
- 3 EL Sonnenblumenöl
- 2 Zehen Knoblauch (sehr fein gehackt)
- 2 Stangen Stangensellerie
- 250g fein gewürfelter Knollensellerie
- 2 Zwiebeln fein gehackt (400g)
- 500g Paprika (in feine Streifen geschnitten)
- 1 EL Paprika edelsüß
- 1 TL geräuchertes Paprikapulver
- ½ TL gemahlene Chilli
- 2 EL getrockneter Thymian (gerebelt)
- Salz
- Pfeffer

== Zubereitung

=== Sojaschnetzel

. Rotwein, Gemüsebrühe und Sojasauce in einem Topf aufkochen lassen.
. Topf vom Herd nehmen und die Sojaschnetzel in der Brühe 15-20 Minuten  quellen lassen.
. Sojaschnetzel abtropfen lassen und die übrige Brühe auffangen und zur Seite stellen.
. Die abgetropften Sojaschnetzel in reichlich Sonnenblumenöl kräftig anbraten und mit dem flüssigem Rauch (mild) würzen.
. Die angebratenen Sojaschnetzel zur Seite stellen.

=== Gulasch

. In einem großen Topf 3 EL Sonnenblumenöl erhitzen.
. Die Zwiebeln und den Knoblauch anbraten bis die Zwiebeln anfangen zu bräunen.
. Paprika edelsüß und das geräucherte Paprikapulver dazu geben und mit anbraten.
. Knollensellerie dazugeben und kurz mit anbraten. 
. Mit Gemüsebrühe, aufgefangener Brühe vom einweichen der Sojaschnetzel und Worcestersauce ablöschen.
. Stangensellerie dazu geben und 5-10 Minuten köcheln lassen. 
. Passierte Tomaten dazu geben und aufkochen lassen. 
. In Streifen geschnittene Paprika dazu geben.
. Chillipulver und Thymian dazu geben.
. Sauce für 1-2 Stunden auf kleiner Flamme simmern lassen. Falls Flüssigkeit fehlt, weitere Gemüsebrühe zugeben.
. Sojaschnetzel unterrühreren und Gulasch mit Salz und Pfeffer abschmecken.
